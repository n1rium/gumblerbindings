package org.robovm.bindings.gumbler;

import org.robovm.apple.foundation.NSCoder;
import org.robovm.apple.foundation.NSCoding;
import org.robovm.apple.foundation.NSDate;
import org.robovm.apple.foundation.NSNumber;
import org.robovm.apple.foundation.NSObject;
import org.robovm.objc.annotation.NativeClass;
import org.robovm.objc.annotation.Property;
import org.robovm.rt.bro.annotation.ByVal;


@NativeClass
public class GumblerMatchResult extends NSObject implements NSCoding {

	@Property(selector = "matchId")
	public native @ByVal NSNumber matchId();
	
	@Property(selector = "setMatchId:")
	public native void setMatchId(@ByVal NSNumber matchId);
	
	@Property(selector = "participantId")
	public native @ByVal NSNumber participantId();
	
	@Property(selector = "setParticipantId:")
	public native void setParticipantId(@ByVal NSNumber participantId);
	
	@Property(selector = "scoreValue")
	public native @ByVal NSNumber scoreValue();
	
	@Property(selector = "setScoreValue:")
	public native void setScoreValue(@ByVal NSNumber scoreValue);
	
	@Property(selector = "autoLossReason")
	public native String autoLossReason();
	
	@Property(selector = "setAutoLossReason:")
	public native void setAutoLossReason(String reason);
	
	@Property(selector = "scoreString")
	public native String scoreString();
	
	@Property(selector = "setScoreString:")
	public native void setScoreString(String scoreString);
	
	@Property(selector = "date")
	public native @ByVal NSDate date();
	
	@Property(selector = "finishedResult")
	public native boolean finishedResult();
	
	@Property(selector = "setFinishedResult:")
	public native void setFinishedResult(boolean result);
	
	@Property(selector = "gameState")
	public native @ByVal NSCoding gameState(); //TODO - Unsure if this is handled correctly (NSObject<NSCoding>)
	
	@Property(selector = "setGameState:")
	public native void setGameState(@ByVal NSCoding gameState); //TODO - same goes here
	
	@Override
	public void encode(NSCoder aCoder) {
		
	}
}
