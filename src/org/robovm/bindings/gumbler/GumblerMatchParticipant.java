package org.robovm.bindings.gumbler;

import org.robovm.apple.foundation.NSCoder;
import org.robovm.apple.foundation.NSCoding;
import org.robovm.apple.foundation.NSNumber;
import org.robovm.apple.foundation.NSObject;
import org.robovm.apple.foundation.NSURL;
import org.robovm.objc.annotation.NativeClass;
import org.robovm.objc.annotation.Property;
import org.robovm.rt.bro.annotation.ByVal;

@NativeClass
public class GumblerMatchParticipant extends NSObject implements NSCoding {

	@Property(selector = "participantId")
	public native @ByVal NSNumber participantId();
	
	@Property(selector = "setParticipantId:")
	public native void setParticipantId(@ByVal NSNumber participantId);
	
	@Property(selector = "participantUserKey")
	public native @ByVal NSNumber participantUserKey();
	
	@Property(selector = "setParticipantUserKey:")
	public native void setParticipantUserKey(@ByVal NSNumber userKey);
	
	@Property(selector = "participantName")
	public native String participantName();
	
	@Property(selector = "setParticipantName:")
	public native void setParticipantName(String name);
	
	@Property(selector = "participantNameFirstPart")
	public native String participantNameFirstPart();
	
	@Property(selector = "setParticipantNameFirstPart:")
	public native void setParticipantNameFirstPart(String nameFirstPart);
	
	@Property(selector = "participantStake")
	public native @ByVal NSNumber participantStake();
	
	@Property(selector = "setParticipantStake:")
	public native void setParticipantStake(@ByVal NSNumber participantStake);
	
	@Property(selector = "participantCurrencyString")
	public native String participantCurrencyString();
	
	@Property(selector = "setParticipantCurrencyString:")
	public native void setParticipantCurrencyString(String currencyString);
	
	@Property(selector = "participantProfilePicture")
	public native String participantProfilePicture();
	
	@Property(selector = "setParticipantProfilePicture:")
	public native void setParticipantProfilePicture(String profilePicture);
	
	@Property(selector = "participantProfilePictureUrl")
	public native @ByVal NSURL participantProfilePictureUrl();
	
	@Override
	public void encode(NSCoder aCoder) {
		
	}
}
