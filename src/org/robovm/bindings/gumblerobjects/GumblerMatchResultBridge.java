package org.robovm.bindings.gumblerobjects;

import org.robovm.bindings.gumbler.GumblerMatchResult;

/*
 * This is a bridge class between pure java and roboVM.
 * This class was created with a sole purpose of sending data between
 * GumblerBridge class and the Gumbler_iOS class.
 */

public class GumblerMatchResultBridge {
	protected long userKey;
	protected long participantId;
	protected long matchId;
	protected long scoreValue;

	protected String scoreString;
	protected String gameStateString;

	protected boolean finishedResult;
	
	public GumblerMatchResultBridge(GumblerMatchResult result) {
		participantId = result.participantId().longValue();
		matchId = result.matchId().longValue();
		scoreValue = result.scoreValue().longValue();
		scoreString = result.scoreString();
		gameStateString = "";
		finishedResult = result.finishedResult();
	}

	public long getUserKey() {
		return userKey;
	}

	public long getParticipantId() {
		return participantId;
	}

	public long getMatchId() {
		return matchId;
	}

	public long getScoreValue() {
		return scoreValue;
	}

	public String getScoreString() {
		return scoreString;
	}

	public String getGameStateString() {
		return gameStateString;
	}

	public boolean isFinishedResult() {
		return finishedResult;
	}
}
