package org.robovm.bindings.gumbler;

import org.robovm.apple.foundation.NSDictionary;
import org.robovm.apple.foundation.NSObject;
import org.robovm.objc.annotation.Method;
import org.robovm.objc.annotation.NativeClass;
import org.robovm.rt.bro.annotation.ByVal;
import org.robovm.rt.bro.annotation.Pointer;

/*
 * This class is not really used as of now. The reason it 
 * was created was for the "handleAction" delegate in Gumbler
 * But that delegate hasn't been tested, nor bound properly.
 */

@NativeClass
public class GumblerObject extends NSObject {
	
	@Method(selector = "initWithParameter:")
	public native @Pointer long initWithParameter(@ByVal NSDictionary<?,?> params);

}
