package org.robovm.bindings.gumbler;

import org.robovm.apple.foundation.NSError;
import org.robovm.apple.foundation.NSNumber;
import org.robovm.bindings.gumblerobjects.GumblerMatchBridge;
import org.robovm.bindings.gumblerobjects.GumblerMatchResultBridge;
import org.robovm.objc.block.VoidBlock2;

/*
 * GumblerBridge
 * Keeps LibGdx from any roboVM related stuff and 
 * delegates them further to Gumbler class
 * 
 * GDXGumbler <<< GumblerBridge <<< Gumbler
 * GDXGumbler >>> GumblerBridge >>> Gumbler
 * 
 * NOTE: This class surveys as a bridge between pure Java and RoboVM stuff.
 * Since the core project of LibGdx doesnt want to have references to robovm stuff
 * this class was created. This class doesn't implement or extend any RoboVM stuff and that's
 * why it works. So in this class we can use robovm stuff as much as we want, but this is the last stop!
 * Passing data from here to Gumbler_iOS class MUST be done in PURE JAVA e.g NOT NSNumber or NSObject etc...
 */

public class GumblerBridge {
	
	public static GumblerListener listener;

	static GumblerDelegate delegate = new GumblerDelegateAdapter() {
		public void openMatch(GumblerMatch match) {
			GumblerMatchBridge matchBridge = new GumblerMatchBridge(match);
			listener.openMatchCallback(matchBridge);
		};
	};
	 
	public static void create(GumblerListener listener) {
		Gumbler.sharedInstance().setDelegate(delegate);
		GumblerBridge.listener = listener;
	}
	
	public static void startOrInstall() {
		Gumbler.startOrInstallGumbler();
	}
	
	public static void install() {
		Gumbler.installGumbler();
	}
	
	public static void createChallenge() {
		Gumbler.createChallenge();
	}
	
	public static void clearMatch() {
		System.out.println("clear match called in GumblerBridge class!");
		Gumbler.clearMatch();
	}
	
	public static void setGumblerDevelopmentMode(boolean isDevelopmentMode){
		Gumbler.sharedInstance().setTesting(isDevelopmentMode);
	}
	
	public static void reportOldMatchResult() {
		Gumbler.reportOldMatchResult();
	}
	
	public static String getGumblerSdkVersion() {
		return Gumbler.getSdkVersion();
	}
	
	public static boolean hasGumblerInstalled() {
		return Gumbler.hasGumblerInstalled();
	}
	
	public static boolean isGumblerGame() {
		return Gumbler.isGumblerGame();
	}
	
	public static boolean hasOldMatchResult() {
		return Gumbler.hasOldMatchResult();
	}
	
	public static boolean isGumblerEnabled() {
		
		boolean cached = Gumbler.isGumblerEnabled(new VoidBlock2<Boolean, NSError>(){
			@Override
			public void invoke(Boolean a, NSError b) {
				listener.isEnabledCallback(a);
			}
		});
		
		return cached;
	}
	
	public static void reportMatchResultAutoLoss(String reason) {
		Gumbler.reportMatchResultAutoLoss(reason);
	}
	
	public static void reportMatchResult(long scoreValue, String scoreString) {
		GumblerMatch match = Gumbler.getMatch();
		GumblerMatchResult result = match.matchResultWithScore(NSNumber.valueOf(scoreValue), scoreString);
		Gumbler.reportMatchResult(result);
	}
	
	public static void reportOngoingMatchResult(long scoreValue, String scoreString) {
		GumblerMatch match = Gumbler.getMatch();
		GumblerMatchResult result = match.matchResultWithScore(NSNumber.valueOf(scoreValue), scoreString);
		Gumbler.reportOngoingMatchResult(result);
	}
	
	public static void reportOngoingMatchResult(long scoreValue, String scoreString, String gameStateString) {
		GumblerMatchResult result = new GumblerMatchResult();
		result.setScoreValue(NSNumber.valueOf(scoreValue));
		result.setScoreString(scoreString);
		//TODO - fix gamestate
		Gumbler.reportOngoingMatchResult(result);
	}
	
	public static GumblerMatchResultBridge getOldMatchResult() {
		GumblerMatchResultBridge result = new GumblerMatchResultBridge(Gumbler.getOldMatchResult());
		return result;
	}
	
	public static GumblerMatchBridge getMatch() {
		GumblerMatchBridge match = new GumblerMatchBridge(Gumbler.getMatch());
		return match;
	}
	
	public static void sendEvent(String eventName) {
		Gumbler.sendEvent(eventName);
	}
}
