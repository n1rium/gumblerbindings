package org.robovm.bindings.gumbler;

import org.robovm.apple.foundation.NSError;
import org.robovm.apple.foundation.NSObject;
import org.robovm.apple.foundation.NSURL;
import org.robovm.objc.annotation.Block;
import org.robovm.objc.annotation.Method;
import org.robovm.objc.annotation.NativeClass;
import org.robovm.objc.annotation.Property;
import org.robovm.objc.block.VoidBlock2;
import org.robovm.rt.bro.annotation.ByVal;

/*
 * This is the binding for the native Gumbler class
 * properties with weak/assign values MUST have a strongRef
 * to not be faulty GC:ed
 */

@NativeClass
public class Gumbler extends NSObject {
	
	public static boolean isGumblerGame() {
		return getMatch() != null;
	}
	
	@Property(selector = "delegate")
	public native GumblerDelegate getDelegate();
	
	@Property(selector = "setDelegate:", strongRef = true)
	public native void setDelegate(GumblerDelegate delegate);
	
	@Method(selector = "sharedInstance")
	public static native Gumbler sharedInstance();
	
	@Method(selector = "match")
	public static native GumblerMatch getMatch();
	
	@Property(selector = "testing")
	public native boolean isTesting();
	
	@Property(selector = "setTesting:", strongRef = true)
	public native void setTesting(boolean testing);
	
	@Method(selector = "startOrInstallGumbler")
	public static native void startOrInstallGumbler();
	
	@Method(selector = "handleOpenURL:sourceApplication:")
	public static native boolean handleOpenURL(@ByVal NSURL url, String string);
	
	@Method(selector = "handleDidBecomeActive")
	public static native void handleDidBecomeActive();
	
	@Method(selector = "handleWillResignActive")
	public static native void handleWillResignActive();
	
	@Method(selector = "handleDidEnterBackground")
	public static native void handleDidEnterBackground();
	
	@Method(selector = "handleWillEnterForeground")
	public static native void handleWillEnterForeground();
	
	@Method(selector = "reportMatchResult:")
	public static native void reportMatchResult(GumblerMatchResult result);
	
	@Method(selector = "reportMatchResultAutoLoss:")
	public static native void reportMatchResultAutoLoss(String reason);
	
	@Method(selector = "clearMatch")
	public static native void clearMatch();
	
	@Method(selector = "createChallenge")
	public static native void createChallenge();
	
	@Method(selector = "hasGumblerInstalled")
	public static native boolean hasGumblerInstalled();
	
	@Method(selector = "installGumbler")
	public static native void installGumbler();
	
	@Method(selector = "sdkVersion")
	public static native String getSdkVersion();
	
	@Method(selector = "isGumblerEnabled:")
	public static native boolean isGumblerEnabled(@Block VoidBlock2<Boolean, NSError> resultBlock);
	
	/*
	 * Ongoing match result
	 */
	@Method(selector = "hasOldMatchResult")
	public static native boolean hasOldMatchResult();
	
	@Method(selector = "oldMatchResult")
	public static native GumblerMatchResult getOldMatchResult();
	
	@Method(selector = "reportOldMatchResult")
	public static native void reportOldMatchResult();
	
	@Method(selector = "reportOngoingMatchResult:")
	public static native void reportOngoingMatchResult(GumblerMatchResult result);
	
	@Method(selector = "sendEvent:")
	public static native void sendEvent(String eventName);
}
