package org.robovm.bindings.gumbler;

import org.robovm.apple.foundation.NSArray;
import org.robovm.apple.foundation.NSCoder;
import org.robovm.apple.foundation.NSCoding;
import org.robovm.apple.foundation.NSDate;
import org.robovm.apple.foundation.NSDictionary;
import org.robovm.apple.foundation.NSNumber;
import org.robovm.apple.foundation.NSObject;
import org.robovm.objc.annotation.Method;
import org.robovm.objc.annotation.NativeClass;
import org.robovm.objc.annotation.Property;
import org.robovm.rt.bro.annotation.ByVal;

/*
 * GumblerMatch binding
 */

@NativeClass
public class GumblerMatch extends NSObject implements NSCoding {

	@Property(selector = "matchId")
	public native @ByVal NSNumber matchId();
	
	@Property(selector = "setMatchId:")
	public native void setMatchId(@ByVal NSNumber participantId);
	
	@Property(selector = "participantId")
	public native @ByVal NSNumber participantId();
	
	@Property(selector = "setParticipantId:")
	public native void setParticipantId(@ByVal NSNumber participantId);
	
	@Property(selector = "participantName")
	public native String participantName();
	
	@Property(selector = "setParticipantName:")
	public native void setParticipantName(String participantName);
	
	@Property(selector = "startDate")
	public native @ByVal NSDate startDate();
	
	@Property(selector = "setStartDate:")
	public native void setStartDate(@ByVal NSDate date);
	
	@Property(selector = "startLevel")
	public native String startLevel();
	
	@Property(selector = "setStartLevel:")
	public native void setStartLevel(String level);
	
	@Property(selector = "seed0")
	public native @ByVal NSNumber seed0();
	
	@Property(selector = "setSeed0:")
	public native void setSeed0(@ByVal NSNumber seed);
	
	@Property(selector = "seed1")
	public native @ByVal NSNumber seed1();
	
	@Property(selector = "setSeed1:")
	public native void setSeed1(@ByVal NSNumber seed);
	
	@Property(selector = "seed2")
	public native @ByVal NSNumber seed2();
	
	@Property(selector = "setSeed2:")
	public native void setSeed2(@ByVal NSNumber seed);
	
	@Property(selector = "extra")
	public native @ByVal NSDictionary<?,?> extra();
	
	@Property(selector = "setExtra:")
	public native void setExtra(@ByVal NSDictionary<?, ?> extra);
	
	@Property(selector = "participants")
	public native @ByVal NSArray<?> participants();
	
	@Property(selector = "setParticipants:")
	public native void setParticipants(NSArray<?> participants);
	
	@Property(selector = "gameId")
	public native @ByVal NSNumber gameId();
	
	@Property(selector = "setGameId:")
	public native void setGameId(@ByVal NSNumber gameId);
	
	@Property(selector = "stake")
	public native @ByVal NSNumber stake();
	
	@Property(selector = "setStake:")
	public native void setStake(@ByVal NSNumber stake);
	
	@Property(selector = "totalPot")
	public native @ByVal NSNumber totalPot();
	
	@Property(selector = "setTotalPot:")
	public native void setTotalPot(@ByVal NSNumber totalPot);
	
	@Property(selector = "currencyString")
	public native String currencyString();
	
	@Property(selector = "setCurrencyString:")
	public native void setCurrencyString(String currencyString);
	
	@Property(selector = "customStake")
	public native String customStake();
	
	@Property(selector = "setCustomStake:")
	public native void setCustomStake(String customStake);
	
	@Property(selector = "isPublic")
	public native boolean isPublic();
	
	@Property(selector = "isPublicNumber")
	public native @ByVal NSNumber isPublicNumber();
	
	@Property(selector = "setIsPublicNumber")
	public native void setIsPublicNumber(@ByVal NSNumber publicNumber);
	
	@Property(selector = "maxParticipants")
	public native @ByVal NSNumber maxParticipants();
	
	@Property(selector = "setMaxParticipants:")
	public native void setMaxParticipants(@ByVal NSNumber maxParticipants);
	
	@Method(selector = "matchResultWithScore:scoreDescription:")
	public native GumblerMatchResult matchResultWithScore(@ByVal NSNumber scoreValue, String scoreDescription);
	
	@Override
	public void encode(NSCoder aCoder) {
		
	}
}
