package org.robovm.bindings.gumbler;

import org.robovm.apple.foundation.NSObject;
import org.robovm.objc.annotation.NotImplemented;

/*
 * Handles the methods from the GumblerDelegate Interface.
 * If the delegates want to be used, this is the class to extend
 * 
 * NOTE: Extend this and DONT implement the GumblerDelegate Interface!
 */

public class GumblerDelegateAdapter extends NSObject implements GumblerDelegate {
	
	@NotImplemented("openMatch:")
	public void openMatch(GumblerMatch match) {
		throw new UnsupportedOperationException();
	}
	
	@NotImplemented("handleAction:object:")
	public void handleAction(String s, GumblerObject obj){
		throw new UnsupportedOperationException();
	}
}
