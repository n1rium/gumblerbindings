package org.robovm.bindings.gumblerobjects;

import org.robovm.apple.foundation.NSDate;
import org.robovm.bindings.gumbler.GumblerMatch;
import org.robovm.bindings.gumbler.GumblerMatchParticipant;

/*
 * This is a bridge class between pure java and roboVM.
 * This class was created with a sole purpose of sending data between
 * GumblerBridge class and the Gumbler_iOS class.
 */

public class GumblerMatchBridge {

	protected long matchId;
	protected long gameId;
	protected long participantId;
	protected long maxParticipants;
	protected long startDate;
	protected long seed0;
	protected long seed1;
	protected long seed2;
	
	protected String stake;
	protected String participantName;
	protected String startLevel;
	protected String currencyString;
	protected String totalPot;
	protected String customStake;
	
	protected boolean isPublic;
	
	protected GumblerMatchParticipantBridge[] participants;
	
	public GumblerMatchBridge(GumblerMatch match) {
			
		isPublic = match.isPublic();
		
		matchId = match.matchId().longValue();
		gameId = match.gameId().longValue();
		participantId = match.participantId().longValue();
		maxParticipants = isPublic ? match.maxParticipants().longValue() : -1;
				
		NSDate date = match.startDate();
		long l = (long) date.getTimeIntervalSince1970() * 1000;
		startDate = l;
				
		seed0 = match.seed0().longValue();
		seed1 = match.seed1().longValue();
		seed2 = match.seed2().longValue();
		stake = String.valueOf(match.stake().longValue());
				
		participantName = match.participantName();
		startLevel = match.startLevel();
		currencyString = match.currencyString();
		totalPot = String.valueOf(match.totalPot().longValue());
		customStake = match.customStake();
		
		participants = new GumblerMatchParticipantBridge[match.participants().size()];
		for(int i = 0; i < match.participants().size(); i++) {
			participants[i] = new GumblerMatchParticipantBridge((GumblerMatchParticipant)match.participants().get(i));
		}			
	}
	
	public void bind(GumblerMatch match) {

	}

	public long getMatchId() {
		return matchId;
	}

	public long getGameId() {
		return gameId;
	}

	public long getParticipantId() {
		return participantId;
	}

	public long getMaxParticipants() {
		return maxParticipants;
	}

	public long getStartDate() {
		return startDate;
	}

	public long getSeed0() {
		return seed0;
	}

	public long getSeed1() {
		return seed1;
	}

	public long getSeed2() {
		return seed2;
	}

	public String getStake() {
		return stake;
	}

	public String getParticipantName() {
		return participantName;
	}

	public String getStartLevel() {
		return startLevel;
	}

	public String getCurrencyString() {
		return currencyString;
	}

	public String getTotalPot() {
		return totalPot;
	}

	public String getCustomStake() {
		return customStake;
	}

	public boolean isPublic() {
		return isPublic;
	}

	public GumblerMatchParticipantBridge[] getParticipants() {
		return participants;
	}
	
	public void log() {
		System.out.println("MatchId: " + matchId);
		System.out.println("GameId: " + gameId);
		System.out.println("ParticipantId: " + participantId);
		System.out.println("MaxParticipants: " + maxParticipants);
		System.out.println("ParticipantName: " + participantName);
		System.out.println("StartDate: " + startDate);
		System.out.println("Seed0: " + seed0);
		System.out.println("Seed1: " + seed1);
		System.out.println("Seed2: " + seed2);
		System.out.println("Stake: " + stake);
		System.out.println("StartLevel: " + startLevel);
		System.out.println("CurrencyString: " + currencyString);
		System.out.println("TotalPot: " + totalPot);
		System.out.println("CustomStake: " + customStake);
		System.out.println("IsPublic: " + isPublic);
	}
}
