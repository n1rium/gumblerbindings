package org.robovm.bindings.gumbler;

import org.robovm.apple.foundation.NSObjectProtocol;
import org.robovm.objc.annotation.Method;
import org.robovm.rt.bro.annotation.Library;

/*
 * Delegate bound from native iOS to handle open match and handle action delegate methods.
 * For now, openMatch is the only one tested and bound properly.
 * 
 * See GumblerDelegateAdapter for implementation of this Interface. Since ios have optional
 * delegates the adapter takes care of this through the @NotImplemented annotation. 
 * Both this Interface and the Adapter class are necessary for this to work.
 */

@Library(Library.INTERNAL)
public interface GumblerDelegate extends NSObjectProtocol {
	
	@Method(selector = "openMatch:")
	void openMatch(GumblerMatch match);
	
	@Method(selector = "handleAction:object:")
	void handleAction(String action, GumblerObject object);
}
