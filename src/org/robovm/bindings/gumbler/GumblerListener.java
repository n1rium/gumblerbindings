package org.robovm.bindings.gumbler;

import org.robovm.bindings.gumblerobjects.GumblerMatchBridge;

/*
 * This Interface works as a delegate Interface
 * Since LibGdx core projects dont want to have anything to do
 * with robovm stuff we have to create a parameterized way to send data
 * which makes stuff pretty messy for us developers, but the users wont
 * be affected. 
 * 
 * NOTE: This could change in upcoming versions. This works, but could
 * probably be optimized...
 */

public interface GumblerListener {
	public void openMatchCallback(GumblerMatchBridge match);
	void isEnabledCallback(boolean isEnabled);
}
