package org.robovm.bindings.gumblerobjects;

import org.robovm.bindings.gumbler.GumblerMatchParticipant;

/*
 * This is a bridge class between pure java and roboVM.
 * This class was created with a sole purpose of sending data between
 * GumblerBridge class and the Gumbler_iOS class.
 */

public class GumblerMatchParticipantBridge {
	protected long id;
	protected long userKey;
	
	protected String currencyString;
	protected String name;
	protected String nameFirstPart;
	protected String profilePicture;
	protected String stake;
	
	public GumblerMatchParticipantBridge(GumblerMatchParticipant p) {
		id = p.participantId().longValue();
		userKey = p.participantUserKey().longValue();
		
		currencyString = p.participantCurrencyString();
		name = p.participantName();
		nameFirstPart = p.participantNameFirstPart();
		profilePicture = p.participantProfilePicture();
		stake = String.valueOf(p.participantStake().longValue());
	}

	public long getId() {
		return id;
	}

	public long getUserKey() {
		return userKey;
	}

	public String getCurrencyString() {
		return currencyString;
	}

	public String getName() {
		return name;
	}

	public String getNameFirstPart() {
		return nameFirstPart;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public String getStake() {
		return stake;
	}
	
	
}
