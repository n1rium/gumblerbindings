package org.robovm.bindings.gumbler.sample;

import org.robovm.apple.foundation.NSAutoreleasePool;
import org.robovm.apple.foundation.NSObject;
import org.robovm.apple.foundation.NSURL;
import org.robovm.apple.uikit.UIApplication;
import org.robovm.apple.uikit.UIApplicationDelegateAdapter;
import org.robovm.bindings.gumbler.Gumbler;

/*
 * This Sample just shows the Gumbler handling methods
 * that takes place in the "core" iOS file IOSLauncher.
 * Make sure to use these so that Gumbler can communicate
 * with your game!
 */

public class Sample extends UIApplicationDelegateAdapter {
    @Override
    public void didFinishLaunching (UIApplication application) {
    }

    public static void main (String[] argv) {
        NSAutoreleasePool pool = new NSAutoreleasePool();
        UIApplication.main(argv, null, Sample.class);
        pool.close();
    }
    
    @Override
    public void didBecomeActive(UIApplication application) {
    	Gumbler.handleDidBecomeActive();
    	super.didBecomeActive(application);
    }
    
    @Override
    public void willResignActive(UIApplication application) {
    	Gumbler.handleWillResignActive();
    	super.willResignActive(application);
    }
    
    @Override
    public boolean openURL(UIApplication application, NSURL url,
    		String sourceApplication, NSObject annotation) {
    	Gumbler.handleOpenURL(url, sourceApplication);
    	return super.openURL(application, url, sourceApplication, annotation);
    }
    
    @Override
    public void didEnterBackground(UIApplication application) {
    	Gumbler.handleDidEnterBackground();
    	super.didEnterBackground(application);
    }
}
